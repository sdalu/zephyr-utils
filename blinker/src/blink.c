/*
 * Copyright (c) 2018,2023 Stéphane D'Alu
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <assert.h>

#include <version.h>

#if KERNEL_VERSION_NUMBER >= 0x030100
#include <zephyr/kernel.h>
#else
#include <kernel.h>
#endif

#include "blinker/blink.h"

#define BLINK_SCHEDULED_DISABLED                0

#define BLINK_FLG_FIRST                         0x01

#define BLINK_TIME_FOREVER                      0xffff

static inline void blink_schedule_next_sequence(blink_t *blink);

/*
 * Reschedule
 *
 * @param blink                 blink handler
 * @param delay                 new delay
 */
static inline void
blink_reschedule(blink_t *blink, uint16_t delay) {
    struct k_work_q         *work_q         = blink->work_q;
    struct k_work_delayable *work_delayable = &blink->onoff_delayable_work;
    k_work_cancel_delayable(work_delayable);
    k_work_schedule_for_queue(work_q, work_delayable, K_MSEC(delay));
}

/*
 * Schedule the next on/off for a blink code.
 *
 * @param blink                 blink handler
 * @param on                    pointer on time for 'on' state
 * @param off                   pointer on time for 'off' state
 *
 * @return if the sequence is to be continued
 */
static bool
blink_onoff_code(blink_t *blink, uint16_t *on, uint16_t *off)
{
    uint8_t count;
    blink_code_t code;

    /* Data */
    code = (blink_code_t) blink->running.data;

    /* Special case */
    switch(code) {
    case BLINK_ON:
        *on  = BLINK_TIME_FOREVER;
        return blink->running.step++ <= 0;

    case BLINK_OFF:
        *off = BLINK_TIME_FOREVER;
        return blink->running.step++ <= 0;
    }

    /* Get count */
    count = BLINK_GET_COUNT(code);
    
    /* Check if aborting blinking sequence is requested */
    if (blink->next.scheduled) {
        if ((count == 0                  ) ||     /* Continuous blink */
            (count == blink->running.step) ||     /* End of group     */
            (blink->next.scheduled == BLINK_SCHEDULED_WAIT_BLINK)) {
            return false;
        }
    }
    
    /* Off state */
    if (count == 0) {                                   /* Continuous blink */
        *off = BLINK_GET_DELAY(code);
    } else if (count == blink->running.step++) {        /* Streak ended     */
        if ((*off = BLINK_GET_WAIT(code))) {
            blink->running.step = 1;
        } else {
            return false;
        }
    } else {                                            /* Streak           */
        *off = BLINK_GET_DELAY(code);
    }
    
    /* On state */
    *on = BLINK_GET_LENGTH(code);

    return true;
}

/*
 * Schedule the next on/off for a morse
 *
 * @param blink                 blink handler
 * @param on                    pointer on time for 'on' state
 * @param off                   pointer on time for 'off' state
 *
 * @return if the sequence is to be continued
 */
static bool
blink_onoff_dotdash(blink_t *blink, uint16_t *on, uint16_t *off)
{
    char *data;

    /* Data */
    data = (char *) blink->running.data;

    /* Check if aborting blinking sequence is requested */
    if (blink->next.scheduled == BLINK_SCHEDULED_WAIT_BLINK) {
        return false;
    }

    /* Dots and Dashes */
 next:
    switch(data[blink->running.step++]) {
    case '.' : *on = 1; *off = 1; break;
    case '-' : *on = 3; *off = 1; break;
    case '\0': return false;
    default:
        goto next;
    }

    return true;
}

/*
 * Mark the blink handle as stopped (ie: not running).
 *
 * @param blink                 blink handler
 */
static inline void
blink_mark_stopped(blink_t *blink, bool forced)
{
    blink->running.onoff = NULL;
    blink->last_time     = forced ? -1 : k_uptime_get();
}

/*
 * Schedule callouts for next on+off.
 *
 * @param blink                 blink handler
 */
static void
blink_schedule_next_onoff(blink_t *blink) 
{
    uint16_t b_on, b_off;
    /* Check for next blink */
    if (blink->running.onoff(blink, &b_on, &b_off)) {
        /* Infinite on */
        if (b_on  == BLINK_TIME_FOREVER) {
            blink->c_state    = true;
            goto infinite;
            
        /* Infinite off */
        } else if (b_off == BLINK_TIME_FOREVER) {
            blink->c_state    = false;
        infinite:
            blink->c_next_off = 0;
	    blink_reschedule(blink, 0);

        /* Normal case */
        } else {
            /* Compute delay for the first callout 
             */
            int64_t delay;

            /* Special handling for first run */
            if (blink->running.flags & BLINK_FLG_FIRST) {
                blink->running.flags &= ~BLINK_FLG_FIRST;
                /* Take into account separator time */
                if (blink->last_time != -1) {
                    delay = k_uptime_get() - blink->last_time;
                    if (delay < (blink->separator * blink->time_unit)) {
                        delay = (blink->separator * blink->time_unit) - delay;
                        goto callouts;
                    }
                }
                /* Directly move to 'on' state */
                delay = 0;
            } else {
                delay = b_off * blink->time_unit;
            }

            /* Set callouts for on+off 
             */
        callouts:
            blink->c_state    = true;
            blink->c_next_off = b_on * blink->time_unit;
	    blink_reschedule(blink, delay);
        }
    /* No more blink */
    } else {
        /* Mark blink as stopped */
        blink_mark_stopped(blink, false);

        /* Check if a new sequence is pending 
         *   (if onoff=NULL is scheduled, that's the stop sequence)
         */
        if (blink->next.scheduled && blink->next.onoff)
            blink_schedule_next_sequence(blink);
    }
}

/*
 * Schedule next blinking sequence.
 */
static inline void
blink_schedule_next_sequence(blink_t *blink)
{
    assert(blink->running.onoff == NULL);
    assert(blink->next.onoff    != NULL);

    /* Move from next to running */
    blink->running.onoff  = blink->next.onoff;
    blink->running.data   = blink->next.data;
    blink->running.step   = 0;

    /* Reset next state */
    blink->next.onoff     = NULL;
    blink->next.scheduled = BLINK_SCHEDULED_DISABLED;

    /* Schedule first on+off */
    blink->running.flags |= BLINK_FLG_FIRST;
    blink_schedule_next_onoff(blink);
}

/*
 * Process 'on'/'off' blink event.
 *
 * @param ev                    generated 'on'/'off' event
 */
static void
blink_onoff_work_handler(struct k_work *item)
{
    blink_t *blink = CONTAINER_OF(k_work_delayable_from_work(item),
				  blink_t, onoff_delayable_work);
    blink->set_state(blink->c_state);
    k_mutex_lock(&blink->mutex, K_FOREVER);
    if (blink->c_next_off) {
        if (blink->c_state) {
            blink->c_state = false;
	    blink_reschedule(blink, blink->c_next_off);
        } else {
            blink_schedule_next_onoff(blink);
        }
    } else {
	blink_mark_stopped(blink, false);
    }
    k_mutex_unlock(&blink->mutex);
}

void
blink_init(blink_t *blink)
{
    /* Ensure we got a reasonable default value for time_unit */
    if (blink->time_unit == 0) {
        blink->time_unit = CONFIG_BLINK_TIME_UNIT;
    }
    if (blink->work_q == NULL) {
	blink->work_q = &k_sys_work_q;
    }
    
    /* Initialize mutex */
    k_mutex_init(&blink->mutex);

    /* Initialize delayable work */
    k_work_init_delayable(&blink->onoff_delayable_work,
			  blink_onoff_work_handler);

    /* Last time */
    blink->last_time = -1;
}

/**
 * Schedule blinking sequence.
 *
 * @param blink                 blink handler
 * @param onoff                 on/off function
 * @param data                  description for the on/off sequence
 * @param scheduled             when to start the blinking sequence:
 *                                o BLINK_SCHEDULED_IMMEDIATE
 *                                o BLINK_SCHEDULED_WAIT_BLINK
 *                                o BLINK_SCHEDULED_WAIT_SEQUENCE
 */
static void
blink_schedule(blink_t *blink, blink_onoff_t onoff, void *data, int scheduled)
{
    k_mutex_lock(&blink->mutex, K_FOREVER);

    /* Set Next */
    blink->next.onoff     = onoff;
    blink->next.data      = data;
    blink->next.scheduled = scheduled;
    
    /* If immediate, force a stop */
    if (scheduled == BLINK_SCHEDULED_IMMEDIATE) {
        blink->c_state    = false;
        blink->c_next_off = 0;
	blink_reschedule(blink, 0);
        blink_mark_stopped(blink, true);
    }

    /* If no blink is running, start it now.
     * Otherwise it is pending and will be started when apropriate
     */
    if ((blink->running.onoff == NULL) &&
        (blink->next.onoff    != NULL)) {
        blink_schedule_next_sequence(blink);
    }

    k_mutex_unlock(&blink->mutex);
}

void
blink_code(blink_t *blink, blink_code_t code, int scheduled)
{
    assert(sizeof(blink_code_t) <= sizeof(void*));
    assert(scheduled != BLINK_SCHEDULED_DISABLED);
    blink_schedule(blink, blink_onoff_code, (void*)code, scheduled);
}

void
blink_dotdash(blink_t *blink, char *dotdash, int scheduled)
{
    assert(scheduled != BLINK_SCHEDULED_DISABLED);

    blink_schedule(blink, blink_onoff_dotdash, dotdash, scheduled);
}

void
blink_stop(blink_t *blink, int scheduled)
{
    assert(scheduled != BLINK_SCHEDULED_DISABLED);
    
    blink_schedule(blink, NULL, NULL, scheduled);
}

