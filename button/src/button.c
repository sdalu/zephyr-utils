/*
 * Copyright (c) 2018,2023 Stéphane D'Alu
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <assert.h>

#include <version.h>

#if KERNEL_VERSION_NUMBER >= 0x030100
#include <zephyr/kernel.h>
#else
#include <kernel.h>
#endif

#include "button/button.h"

#define _BUTTON_FSM_INIT                        0
#define _BUTTON_FSM_PRESSED                     1
#define _BUTTON_FSM_WAIT_DBLPRESSED             2
#define _BUTTON_FSM_DBLPRESSED                  3
#define _BUTTON_FSM_HOLD_OR_REPEAT              4

#define _BUTTON_RELEASED                        0
#define _BUTTON_PRESSED                         1
#define _BUTTON_TIMEOUT                         2

struct button_event {
    struct k_work work;
    button_t *button;
    uint8_t type;
    uint8_t flags;
#define BUTTON_EVENT_INUSE			0x01
    atomic_t iflags;
};


static inline void
button_delayable_work_submit(button_t *btn, int32_t delay) {
    struct k_work_q *work_q = &k_sys_work_q;
    k_work_schedule_for_queue(work_q, &btn->delayable_work, K_MSEC(delay));
}

static inline void
button_delayable_work_cancel(button_t *btn)
{
    k_work_cancel_delayable(&btn->delayable_work);
}



#define BUTTON_POST_STATE_EVENT(button, flags)  \
    button_post_event(button, BUTTON_STATE_CHANGED, flags)

#define BUTTON_POST_ACTION_EVENT(button, flags) \
    button_post_event(button, BUTTON_ACTION, flags)

static struct button_event button_event[CONFIG_BUTTON_EVENT_MAX] = { 0 };
static button_callback_t button_callback = NULL;

static struct button_event *
button_alloc_event(void)
{
    int i;

    for (i = 0 ; i < CONFIG_BUTTON_EVENT_MAX ; i++)
        if (! atomic_test_and_set_bit(&button_event[i].iflags,
				      BUTTON_EVENT_INUSE))
	    return &button_event[i];

    return NULL;
}

/**
 * Process generated event, using the user defined callback.
 *
 * @param item		work item
 */
static void
button_work_handler(struct k_work *item)
{
    struct button_event *b_ev = CONTAINER_OF(item, struct button_event, work);
    button_callback(b_ev->button->id, b_ev->type, b_ev->flags);
    atomic_clear(&b_ev->iflags);
}

/**
 * Post an event to the default queue.
 * 
 * @param button        button generating the event
 * @param type          type of event (state or action)
 * @param flags         event description (BUTTON_FLG_*)
 *
 * @return              -1 unable to post event (not enough buffer)
 *                       0 event posted
 *                       1 event not posted due to filtering
 */
static int
button_post_event(button_t *button, uint8_t type, uint8_t flags)
{
    struct button_event *evt;

#if defined(CONFIG_BUTTON_USE_FILTERING)
    if (button->filter.enabled) {
        uint8_t filter = 0xff;
        switch(type) {
#if defined(CONFIG_BUTTON_EMIT_STATE_CHANGED)
        case BUTTON_STATE_CHANGED:
            filter = button->filter.state;
            break;
#endif
#if defined(CONFIG_EMIT_ACTION)
        case BUTTON_ACTION:
            filter = button->filter.action;
            break;
#endif
        }
        if (~filter & flags) {
            return 1;
        }
    }
#endif
    evt = button_alloc_event();
    if (evt) {
	evt->button = button;
        evt->type   = type;
        evt->flags  = flags;
	k_work_init(&evt->work, button_work_handler);
#if defined(CONFIG_BUTTON_USE_PER_BUTTON_WORK_Q)
    k_work_submit_to_queue(button->work_q, &evt->work);
#else
    k_work_submit_to_queue(&k_sys_work_q, &evt->work);
#endif
        return 0;
    } else {
        button->state |= BUTTON_FLG_MISSED;
        return -1;
    }
}

#if defined(CONFIG_BUTTON_USE_EMULATION)
/**
 * Perform button emulation.
 *
 * @param button        the button to emulate
 */
static void
button_emulating(button_t *button)
{
    bool pressed  = true;
    bool released = true;
    
    for (button_t **from = button->emulated ; *from ; from++) {
        if ((*from)->state & BUTTON_FLG_PRESSED) {
            released = false;
        } else {
            pressed  = false;
        }
    }
    
    if (pressed != !released)
        return;

    if (pressed || (released && button->emulating)) {
        button->emulating = pressed;
        button_set_low_level_state(button, pressed);
    }
}

/**
 * Process children of a button. Used for emulating button.
 *
 * @param button        the button to look for children
 */
static void
button_process_children(button_t *button)
{
    if (button->children == NULL)
        return;

    for (button_t **child = button->children ; *child ; child++)
        if ((*child)->emulated)
            button_emulating(*child);
}

/**
 * Check if one of the children is using the action, and so
 * we shouldn't generate one for ourself.
 *
 * @param button        the button to look for children
 */
static bool
button_stealed_action(button_t *button)
{
    if (button->children == NULL)
        return false;

    for (button_t **child = button->children ; *child ; child++)
        if ((*child)->emulated && (*child)->emulating)
            return true;

    return false;
}
#endif

/**
 * Use low level action, to generated button state and action.
 * This one deal with simple state/action, where only pressed/released/click
 * are considered.
 *
 * @param button        button to process
 * @param action        low level action associated to the button
 *                      (_BUTTON_PRESSED, _BUTTON_RELEASED)
 */
static void
button_exec_simple(button_t *button, int action)
{
    assert(action != _BUTTON_TIMEOUT);

    switch(action) {
    case _BUTTON_PRESSED:
        button->state  =  BUTTON_FLG_PRESSED;
        break;
    case _BUTTON_RELEASED:
#if defined(CONFIG_BUTTON_EMIT_ACTION)
#if defined(CONFIG_BUTTON_USE_EMULATION)
        if (!button_stealed_action(button))
#endif
            BUTTON_POST_ACTION_EVENT(button, button->state);
#endif
        button->state &= ~BUTTON_FLG_PRESSED;
        break;
    }

#if defined(CONFIG_BUTTON_EMIT_STATE_CHANGED)
    BUTTON_POST_STATE_EVENT(button, button->state);
#endif
    
#if defined(CONFIG_BUTTON_USE_EMULATION)
    button_process_children(button);
#endif
}

#if defined(CONFIG_BUTTON_USE_DOUBLE) || \
    defined(CONFIG_BUTTON_USE_LONG  ) || \
    defined(CONFIG_BUTTON_USE_REPEAT)
/**
 * Use low level action, to generated button state and action.
 * This one deal with complexe action, such as generating double click, 
 * long click, repeating action, ...
 *
 * @param button        the button to process
 * @param action        low level action associated to the button
 *                      (_BUTTON_PRESSED, _BUTTON_RELEASED, _BUTTON_TIMEOUT)
 */
static void
button_exec_fsm(button_t *button, int action)
{
    switch (button->fsm_state) {
    case _BUTTON_FSM_INIT:
        switch(action) {
        case _BUTTON_PRESSED:
            goto do_pressed;
        case _BUTTON_RELEASED:
            goto do_nothing;
        case _BUTTON_TIMEOUT:
            goto do_assert;
        }
        break;

    case _BUTTON_FSM_PRESSED:
        switch(action) {
        case _BUTTON_PRESSED:
            goto do_nothing;
        case _BUTTON_RELEASED:
#if defined(CONFIG_BUTTON_USE_DOUBLE)
            if (button->mode & BUTTON_FLG_DOUBLED) {
                goto to_wait_dblpressed;
            } else {
#endif
                goto do_release;
#if defined(CONFIG_BUTTON_USE_DOUBLE)
            }
#endif
        case _BUTTON_TIMEOUT:
#if defined(CONFIG_BUTTON_USE_LONG)
            if (button->mode & BUTTON_FLG_LONG) {
                goto do_longpress;
#if defined(CONFIG_BUTTON_USE_REPEAT)
            } else if (button->mode & BUTTON_FLG_REPEATING) {
                goto do_repeat;
#endif
            } else {
                goto do_assert;
            }
#elif defined(CONFIG_BUTTON_USE_REPEAT)
            goto do_repeat;
#else
            goto do_assert;
#endif
        }
        break;
        
#if defined(CONFIG_BUTTON_USE_DOUBLE)
    case _BUTTON_FSM_WAIT_DBLPRESSED:
        switch(action) {
        case _BUTTON_TIMEOUT:
            goto do_release;
        case _BUTTON_PRESSED:
            goto do_dblpressed;
        case _BUTTON_RELEASED:
            goto do_nothing;
        }
        break;

    case _BUTTON_FSM_DBLPRESSED:
        switch(action) {
        case _BUTTON_TIMEOUT :
#if defined(CONFIG_BUTTON_USE_LONG)
            if (button->mode & BUTTON_FLG_LONG) {
                goto do_longpress;
#if defined(CONFIG_BUTTON_USE_REPEAT)
            } else if (button->mode & BUTTON_FLG_REPEATING) {
                goto do_repeat;
#endif
            } else {
                goto do_assert;
            }
#elif defined(CONFIG_BUTTON_USE_REPEAT)
            goto do_repeat;
#else
            goto do_assert;
#endif
        case _BUTTON_RELEASED: goto do_release;
        case _BUTTON_PRESSED : goto do_nothing;
        } 
        break;
#endif

#if defined(CONFIG_BUTTON_USE_LONG  ) || \
    defined(CONFIG_BUTTON_USE_REPEAT)
    case _BUTTON_FSM_HOLD_OR_REPEAT:
        switch(action) {
        case _BUTTON_TIMEOUT:
#if defined(CONFIG_BUTTON_USE_REPEAT)
            if (button->mode & BUTTON_FLG_REPEATING) {
                goto do_repeat;
            } else {
#endif
                goto do_assert;
#if defined(CONFIG_BUTTON_USE_REPEAT)
            }
#endif
        case _BUTTON_RELEASED:
            goto do_release;
        case _BUTTON_PRESSED:
            goto do_nothing;
        }
        break;
#endif
    }

 do_assert:
    assert(0);
    
 do_nothing:
    return;
    
 do_pressed:
#if defined(CONFIG_BUTTON_USE_LONG)
    if (button->mode & BUTTON_FLG_LONG) {
        button_delayable_work_submit(button, CONFIG_BUTTON_LONGHOLD_TICKS);
#if defined(CONFIG_BUTTON_USE_REPEAT)
    } else if (button->mode & BUTTON_FLG_REPEATING) {
        button_delayable_work_submit(button, CONFIG_BUTTON_REPEAT_FIRST_TICKS);
#endif
    }
#elif defined(CONFIG_BUTTON_USE_REPEAT)
    button_delayable_work_submit(button, CONFIG_BUTTON_REPEAT_FIRST_TICKS);
#endif
    button->state = BUTTON_FLG_PRESSED;
#if defined(CONFIG_BUTTON_EMIT_STATE_CHANGED)
    BUTTON_POST_STATE_EVENT(button, button->state);
#endif
#if defined(CONFIG_BUTTON_USE_EMULATION)
    button_process_children(button);
#endif
    button->fsm_state = _BUTTON_FSM_PRESSED;
    return;
    
#if defined(CONFIG_BUTTON_USE_DOUBLE)
 to_wait_dblpressed:
    button_delayable_work_submit(button, CONFIG_BUTTON_DBLCLICK_TICKS);
    button->fsm_state = _BUTTON_FSM_WAIT_DBLPRESSED;
    return;
    
 do_dblpressed:
#if defined(CONFIG_BUTTON_USE_LONG)
    if (button->mode & BUTTON_FLG_LONG) {
        button_delayable_work_submit(button, CONFIG_BUTTON_LONGHOLD_TICKS);
#if defined(CONFIG_BUTTON_USE_REPEAT)
    } else if (button->mode & BUTTON_FLG_REPEATING) {
        button_delayable_work_submit(button, CONFIG_BUTTON_REPEAT_FIRST_TICKS);
#endif
    } else {
        button_delayable_work_cancel(button);
    }   
#elif defined(CONFIG_BUTTON_USE_REPEAT)
    button_delayable_work_submit(button, CONFIG_BUTTON_REPEAT_FIRST_TICKS);
#else
    button_delayable_work_cancel(button);
#endif
    button->state |= BUTTON_FLG_DOUBLED;
#if defined(CONFIG_BUTTON_EMIT_STATE_CHANGED)
    BUTTON_POST_STATE_EVENT(button, button->state);
#endif
    button->fsm_state = _BUTTON_FSM_DBLPRESSED;
    return;
#endif

#if defined(CONFIG_BUTTON_USE_LONG)
 do_longpress:
#if defined(CONFIG_BUTTON_USE_REPEAT)
    if (button->mode & BUTTON_FLG_REPEATING)
        button_delayable_work_submit(button, CONFIG_BUTTON_REPEAT_FIRST_TICKS);
#endif
    button->state |= BUTTON_FLG_LONG;
#if defined(CONFIG_BUTTON_EMIT_STATE_CHANGED)
    BUTTON_POST_STATE_EVENT(button, button->state);
#endif
    button->fsm_state = _BUTTON_FSM_HOLD_OR_REPEAT;
    return;
#endif
    
#if defined(CONFIG_BUTTON_USE_REPEAT)
 do_repeat:
    button_delayable_work_submit(button, CONFIG_BUTTON_REPEAT_TICKS);
    
#if defined(CONFIG_BUTTON_EMIT_ACTION)
#if defined(CONFIG_BUTTON_USE_EMULATION)
    if (!button_stealed_action(button))
#endif
        BUTTON_POST_ACTION_EVENT(button, button->state);
#endif
    
    if (!(button->state & BUTTON_FLG_REPEATING)) {
        button->state |= BUTTON_FLG_REPEATING;
#if defined(CONFIG_BUTTON_EMIT_STATE_CHANGED)
        BUTTON_POST_STATE_EVENT(button, button->state);
#endif
    }
    return;
#endif
    
 do_release:
    button_delayable_work_cancel(button);

#if defined(CONFIG_BUTTON_EMIT_ACTION)
#if defined(CONFIG_BUTTON_USE_EMULATION)
    if (!button_stealed_action(button)) {
#endif
#if defined(CONFIG_BUTTON_USE_REPEAT)
        if (!(button->state & BUTTON_FLG_REPEATING))
#endif
            BUTTON_POST_ACTION_EVENT(button, button->state);
#if defined(CONFIG_BUTTON_USE_EMULATION)
    }
#endif
#endif
    button->state &= ~BUTTON_FLG_PRESSED;
#if defined(CONFIG_BUTTON_EMIT_STATE_CHANGED)
    BUTTON_POST_STATE_EVENT(button, button->state);
#endif
#if defined(CONFIG_BUTTON_USE_EMULATION)
    button_process_children(button);
#endif
    button->fsm_state = _BUTTON_FSM_INIT;
    return;
}


/**
 * Work item handler used for processing timeout.
 * Used for processing complexe action.
 *
 * @param item          work item
 */
static void
button_fsm_work_handler(struct k_work *item)
{
    button_t *button = CONTAINER_OF(k_work_delayable_from_work(item),
				    button_t, delayable_work);
    button_exec_fsm(button, _BUTTON_TIMEOUT);
}
#endif

void
button_init(button_t *buttons, unsigned int count, button_callback_t cb)
{
#if defined(CONFIG_BUTTON_USE_DOUBLE) ||                            \
    defined(CONFIG_BUTTON_USE_LONG  ) ||                            \
    defined(CONFIG_BUTTON_USE_REPEAT) ||                            \
    defined(CONFIG_BUTTON_USE_PER_BUTTON_WORK_Q)
    unsigned int i;
#endif
        
    button_callback = cb;

#if defined(CONFIG_BUTTON_USE_DOUBLE) || \
    defined(CONFIG_BUTTON_USE_LONG  ) || \
    defined(CONFIG_BUTTON_USE_REPEAT)
    for (i = 0 ; i < count ; i++) {
        button_t *button = &buttons[i];
        k_work_init_delayable(&button->delayable_work, button_fsm_work_handler);
    }
#endif

#if defined(CONFIG_BUTTON_USE_PER_BUTTON_WORK_Q)
    for (i = 0 ; i < count ; i++) {
        button_t *button = &buttons[i];
        if (button->work_q == NULL) {
            button->work_q = &k_sys_work_q;
        }
    }
#endif
}

void
button_set_low_level_state(button_t *button, bool pressed)
{
    int action;

    action = pressed ? _BUTTON_PRESSED : _BUTTON_RELEASED;
#if defined(CONFIG_BUTTON_USE_DOUBLE) || \
    defined(CONFIG_BUTTON_USE_LONG  ) || \
    defined(CONFIG_BUTTON_USE_REPEAT)
    if (button->mode & ~(BUTTON_FLG_PRESSED)) {
        button_exec_fsm(button, action);
    } else {
        button_exec_simple(button, action);
    }
#else
    button_exec_simple(button, action);
#endif
}

