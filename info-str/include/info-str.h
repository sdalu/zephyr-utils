#ifndef __INFO_STR__H
#define __INFO_STR__H

#include <version.h>

#if KERNEL_VERSION_NUMBER >= 0x030100
#include <zephyr/kernel.h>
#else
#include <zephyr.h>
#endif

#if defined(CONFIG_SHELL)
#if KERNEL_VERSION_NUMBER >= 0x030100
#include <zephyr/shell/shell.h>
#else
#include <shell/shell.h>
#endif

int info_str_cmd_os(const struct shell *shell, size_t argc, char **argv);
int info_str_cmd_build(const struct shell *shell, size_t argc, char **argv);
int info_str_cmd_mcu(const struct shell *shell, size_t argc, char **argv);

#endif

#endif
